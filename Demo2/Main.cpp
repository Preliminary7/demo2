// Name: Xao Thao
// Date: 08/28/2019
// Project Name: Demo 2 (Loop, course control)
// Project description: How to loop 


#include <iostream>
#include <conio.h>

using namespace std;


int main()
{

/*
	int i = 0;

	for (int i = 0; i < 10; i++)
	{
		cout << i << "\n";
	}
*/

	// cout << i; (if this is left on then the code will print 0 at the end of 9 for the for loop instead of 10)




/*
	cout << "Would you like to do math? (y/n): ";
	char input;
	int x = 1;
	cin >> input;

	while (input == 'y')
	{
		cout << "9 x " << x << " = " << 9 * x;
		x++;
		cout << " Another? (y/n): ";
		cin >> input;
	}

	char input;
	int x = 1;
*/

	/*

	do
	{
		cout << "9 x " << x << " = " << 9 * x << "\n";
		x++;
		cout << " Another? (y/n): ";
		cin >> input;

	} while (input == 'y');
	{
		cout << "9 x " << x << " = " << 9 * x;
		x++;
		cout << " Another? (y/n): ";
		cin >> input;
	}
	
	*/

	cout << "Enter an interger: ";
	int input;
	cin >> input;

	// even or odd...
	/*
	if (input % 2 == 0) cout << input << " is odd. \n";
	else cout << input << " is even. \n";
	*/

	// shorter way to code. Print out even or odd.
	cout << input << " is " << ((input % 2) ? "odd" : "even") << ".\n";

/*
	if (input % 2 == 0)
	{
		cout << input << " is even. \n";
	}
	else
	{
		cout << input << " is odd. \n";
	}
*/

	_getch();
	return 0;

}